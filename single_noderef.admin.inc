<?php
// $Id$

/**
 * @file
 * Admin settings
 */

/**
 * Menu handler
 */
function single_noderef_settings() {
  $form = array();
  $options = single_noderef_avilable_fields();
  if (count($options)) {
    $form['single_noderef_fields'] = array(
      '#type' => 'select',
      '#title' => t('Fields'),
      '#options' => $options,
      '#multiple' => TRUE,
      '#default_value' => variable_get('single_noderef_fields', array()),
      '#description' => t('Choose one or more fields'),
    );
  } 
  else {
    drupal_set_message(t('There are no nodereference fields'));
  }
  return system_settings_form($form);
}

/**
 * Return an array of existing node refference fields
 */
function single_noderef_avilable_fields() {
  $fields = content_fields();
  $options = array();
  foreach ($fields as $field) {
    if ($field['type'] == 'nodereference') {
      $options[$field['field_name']] = $field['widget']['label'];
    }
  }
  // Sort the list by type, then by field name, then by label.
  asort($options);

  return $options;
}
